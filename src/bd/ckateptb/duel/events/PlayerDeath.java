package bd.ckateptb.duel.events;

import bd.ckateptb.duel.main.DuelMe;
import bd.ckateptb.duel.mysql.FieldName;
import bd.ckateptb.duel.mysql.MySql;
import bd.ckateptb.duel.util.DuelManager;
import bd.ckateptb.duel.util.FileManager;
import bd.ckateptb.duel.util.MessageManager;
import bd.ckateptb.duel.util.SendConsoleMessage;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;

import java.util.UUID;

/**
 * Created by Frank on 04/05/2015.
 */
public class PlayerDeath implements Listener {

    private DuelMe plugin;

    public PlayerDeath(DuelMe plugin) {
        this.plugin = plugin;
        plugin.getServer().getPluginManager().registerEvents(this, plugin);
    }

    @EventHandler (priority = EventPriority.NORMAL)
    public void onPlayerDeath(PlayerDeathEvent e) {
        if(plugin.isDebugEnabled()) {
            SendConsoleMessage.debug("Death Event Fired");
        }
        Player loser = e.getEntity();
        String playerName = loser.getName();
        UUID playerUUID = loser.getUniqueId();

        DuelManager dm = plugin.getDuelManager();
        FileManager fm = plugin.getFileManager();
        MySql mySql = plugin.getMySql();
        MessageManager mm = plugin.getMessageManager();
        if(dm.isInDuel(playerUUID)){
            dm.addDeadPlayer(playerUUID);

            if(fm.isMySqlEnabled()) {
                mySql.addPlayerKillDeath(playerUUID, playerName, FieldName.DEATH);
            }

            if(e.getEntity().getKiller() instanceof Player){
                Player killer = e.getEntity().getKiller();
                String killerName = killer.getName();
                if(fm.isMySqlEnabled()) {
                    mySql.addPlayerKillDeath(playerUUID, killerName, FieldName.KILL);
                }

                if(!fm.isDropItemsOnDeathEnabled()) {
                    if(plugin.isDebugEnabled()) {
                        SendConsoleMessage.debug("Item drops disabled, clearing.");
                    }
                    e.getDrops().clear();
                }

                if(!fm.isDeathMessagesEnabled()){
                    e.setDeathMessage("");
                    return;
                }
                String deathMessageByPlayer = mm.getKillMessageByPlayer();
                deathMessageByPlayer = deathMessageByPlayer.replaceAll("%player%", playerName);
                deathMessageByPlayer = deathMessageByPlayer.replaceAll("%killer%", killerName);
                e.setDeathMessage(fm.getPrefix() + " " + deathMessageByPlayer);
            }  else {
                if(plugin.isDebugEnabled()) {
                    SendConsoleMessage.debug("Death cause not by player.");
                }
                if(!fm.isDeathMessagesEnabled()){
                    e.setDeathMessage("");
                    return;
                }

                String deathMessageByOther = mm.getKillOtherMessage();
                deathMessageByOther = deathMessageByOther.replaceAll("%player%", playerName);
                e.setDeathMessage(fm.getPrefix() + " " + deathMessageByOther);
            }
            dm.endDuel(loser);
            if(plugin.getFileManager().isForceRespawnEnabled()) {
                loser.spigot().respawn();
            }

        }
    }

}
