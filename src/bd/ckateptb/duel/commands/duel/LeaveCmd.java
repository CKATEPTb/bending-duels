package bd.ckateptb.duel.commands.duel;

import bd.ckateptb.duel.main.DuelMe;
import bd.ckateptb.duel.util.DuelManager;
import bd.ckateptb.duel.util.Util;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.UUID;

public class LeaveCmd extends DuelCmd {
    public LeaveCmd(DuelMe plugin, String mainPerm) {
        super(plugin, mainPerm);
    }

    @Override
    public void run(CommandSender sender, String subCmd, String[] args) {
        if(!(sender instanceof Player)){
            Util.sendMsg(sender, NO_CONSOLE);
            return;
        }

        Player player = (Player) sender;
        UUID playerUUID = player.getUniqueId();
        DuelManager dm = plugin.getDuelManager();

        if(dm.isInDuel(playerUUID)){
            dm.endDuel(player);
        } else {
            if(dm.isQueued(playerUUID)) {
                dm.removeQueuedPlayer(playerUUID);
                Util.sendMsg(sender, ChatColor.GOLD + "�����, �� ����� �� �������! ���� ���?");
                return;
            }
            Util.sendMsg(sender, ChatColor.RED + "���, �� �� � ������� � ��� ����� �� � ��������!");
        }
    }
}
