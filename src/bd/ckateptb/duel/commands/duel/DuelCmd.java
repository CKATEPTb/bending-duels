package bd.ckateptb.duel.commands.duel;

import bd.ckateptb.duel.commands.SubCmd;
import bd.ckateptb.duel.main.DuelMe;

import org.bukkit.command.CommandSender;

public abstract class DuelCmd extends SubCmd {

    public DuelCmd(DuelMe plugin, String mainPerm) {
        super(plugin, mainPerm);
    }

    public abstract void run(CommandSender sender, String subCmd, String[] args);
}
