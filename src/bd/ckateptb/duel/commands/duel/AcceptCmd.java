package bd.ckateptb.duel.commands.duel;

import bd.ckateptb.duel.main.DuelMe;
import bd.ckateptb.duel.util.DuelManager;
import bd.ckateptb.duel.util.Util;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class AcceptCmd extends DuelCmd {

    public AcceptCmd(DuelMe plugin, String mainPerm) {
        super(plugin, mainPerm);
    }

    @Override
    public void run(CommandSender sender, String subCmd, String[] args) {
        if (!(sender instanceof Player)) {
            Util.sendMsg(sender, NO_CONSOLE);
            return;
        }

        if(args.length < 1){
            Util.sendMsg(sender, ChatColor.GREEN + "���������: /duel accept <�����>");
            return;
        }

        Player accepter = (Player) sender;
        String senderName = getValue(args, 0, "");

        DuelManager dm = plugin.getDuelManager();
        dm.acceptRequest(accepter , senderName);
    }
}
