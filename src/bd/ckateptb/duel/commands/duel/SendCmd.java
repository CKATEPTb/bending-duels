package bd.ckateptb.duel.commands.duel;

import bd.ckateptb.duel.main.DuelMe;
import bd.ckateptb.duel.util.DuelManager;
import bd.ckateptb.duel.util.Util;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class SendCmd extends DuelCmd {

    public SendCmd(DuelMe plugin, String mainPerm) {
        super(plugin, mainPerm);
    }

    @Override
    public void run(CommandSender sender, String subCmd, String[] args) {
        if (!(sender instanceof Player)) {
            Util.sendMsg(sender, NO_CONSOLE);
            return;
        }

        if(args.length < 1){
            Util.sendEmptyMsg(sender, ChatColor.GREEN + "���������: /duel send <�����>");
            return;
        }

        Player duelSender = (Player) sender;
        String duelTarget = getValue(args, 0, "");
        String arenaNameIn = null;
        DuelManager dm = plugin.getDuelManager();

        if(args.length == 1) {
            dm.sendDuelRequest(duelSender, duelTarget, arenaNameIn);
        } else if( args.length == 2) {
            if(!sender.hasPermission("duelme.player.sendarena")) {
                Util.sendMsg(sender, NO_PERM);
                return;
            }
            arenaNameIn = getValue(args, 1, "");
            dm.sendDuelRequest(duelSender, duelTarget, arenaNameIn);
        }
    }
}
