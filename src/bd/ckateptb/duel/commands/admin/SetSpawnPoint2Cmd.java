package bd.ckateptb.duel.commands.admin;

import bd.ckateptb.duel.main.DuelMe;
import bd.ckateptb.duel.util.DuelArena;
import bd.ckateptb.duel.util.Util;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class SetSpawnPoint2Cmd extends DuelAdminCmd {

    public SetSpawnPoint2Cmd(DuelMe plugin, String mainPerm) {
        super(plugin, mainPerm);
    }

    @Override
    public void run(DuelArena duelArena, CommandSender sender, String subCmd, String[] args) {
        if(!(sender instanceof Player)){
            Util.sendMsg(sender, NO_CONSOLE);
            return;
        }

        String duelArenaName = duelArena.getName();
        Player player = (Player) sender;

        Location pos1 = duelArena.getPos1();
        Location pos2 = duelArena.getPos2();
        Location playerLocation = player.getLocation();

        double x = playerLocation.getBlockX();
        double y = playerLocation.getBlockY();
        double z = playerLocation.getBlockZ();

        if(!Util.isInRegion(playerLocation, pos1, pos2)) {
            Util.sendMsg(sender, ChatColor.translateAlternateColorCodes('&',
                    "&cYou must be inside the region for arena: &b" + duelArenaName + " &cto set a spawnpoint!"));
            return;
        }

        duelArena.setSpawnpoint2(playerLocation);
        Util.sendMsg(sender, ChatColor.translateAlternateColorCodes('&',
                "&aSpawnpoint2 set to: " + "&a(&b" + x + "&a)(&b" + y + "&a)(&b" + z + "&a)"
                        + ChatColor.GREEN + " for arena "  + ChatColor.AQUA + duelArenaName));
    }
}
