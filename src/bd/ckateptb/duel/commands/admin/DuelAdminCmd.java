package bd.ckateptb.duel.commands.admin;

import bd.ckateptb.duel.commands.SubCmd;
import bd.ckateptb.duel.main.DuelMe;
import bd.ckateptb.duel.util.DuelArena;

import org.bukkit.command.CommandSender;

public abstract class DuelAdminCmd extends SubCmd {

    public DuelAdminCmd(DuelMe plugin, String mainPerm) {
        super(plugin, mainPerm);
    }

    public abstract void run(DuelArena duelArena, CommandSender sender, String subCmd, String[] args);
}
